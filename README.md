# BXC_MediaServer

* 作者：北小菜 
* 官网：http://www.beixiaocai.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* 哔哩哔哩主页：https://space.bilibili.com/487906612
* github开源地址：https://github.com/any12345com/BXC_MediaServer
* gitee开源地址：https://gitee.com/Vanishi/BXC_MediaServer


### 项目介绍
~~~
BXC_MediaServer 是基于C++开发的直播服务器项目，对应《从零开始编写一个直播服务器》系列教程源码。
这个系列课程主要会涉及rtmp，http-flv，hls三个直播最常用的流媒体协议。
首先会分别实现这3个协议对应的流媒体服务器，包括简单版和进阶版，每一个协议对应的流媒体服务都会从原理层面通过代码从零进行实现。
（最后）会实现一个完整的包含以上3个协议的直播流媒体服务器。
~~~

### 视频教程地址：
1. [《从零开始编写一个直播服务器》第1讲：实现一个最简单的HLS服务器](https://www.bilibili.com/video/BV1v84y1a7hr) 

2. [《从零开始编写一个直播服务器》第2讲：实现一个最简单的HTTP-FLV服务器](https://www.bilibili.com/video/BV1aM411h7o3) 

3. [《从零开始编写一个直播服务器》第3讲：实现一个最简单的RTMP服务器](https://www.bilibili.com/video/BV1xd4y1W71a) 

4. [《从零开始编写一个直播服务器》第4讲：实现一个高性能的HTTP-FLV流媒体服务器](https://space.bilibili.com/487906612/channel/collectiondetail?sid=1010705) 


### 相关项目介绍
1. [实现浏览器播放HLS和HTTP-FLV视频流-视频教程](https://www.bilibili.com/video/BV1QM411s77Y) 
2. [实现浏览器播放HLS和HTTP-FLV视频流-源码下载](http://www.any12345.com/article.html?id=1) 


### windows编译运行
~~~
作者配置了 x64/Debug 和 x64/Release 的第三方库的依赖环境

注意：第1讲，第2讲，第3讲 三个项目不依赖任何第三方库
	  第4讲项目 依赖boost，jsoncpp两个第三方库，依赖库文件比较大，因此放在了网盘，下载3rdparty.zip解压后放在软件根目录下即可
	  第三方库网盘下载链接：链接：https://pan.quark.cn/s/b835be15750e 提取码：2Jhm
	  
~~~




